import { createStore, applyMiddleware , compose } from "redux";
import createSagaMiddleware from "redux-saga";

import {rootReducer} from '../redux/reducers/root.reducer'
import rootSaga from "../saga/root.saga";
const sagaMiddleware = createSagaMiddleware();
// export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
export const store = compose(
    applyMiddleware(sagaMiddleware),
    window.devToolsExtension && window.devToolsExtension(),
)(createStore)(rootReducer)
sagaMiddleware.run(rootSaga)


