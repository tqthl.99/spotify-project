import * as types from "../../type/typeAction";

export const postPlayMusicHomeReducer = (state = "", action) => {
  switch (action.type) {
    case types.POST_PLAY_SONG_HOME_TO_STORE:
      return action.payload;
    default:
      return state;
  }
};
