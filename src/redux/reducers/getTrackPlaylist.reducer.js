import * as types from "../../type/typeAction";
// const initialState = {
//     id: Number,
//     playlistsItem: String,
//     namePlaylistInput: String,
//     avatarPlaylist: String,
// }
export const getTrackPlaylistReducer = (state = {}, action) => {
  switch (action.type) {
    case types.GET_OFFER_TRACK_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};
export const postTrackPlaylistReducer = (state = [], action) => {
  switch (action.type) {
    case types.POST_TRACK_TO_PLAYLIST_SUCCESS:
      state.push(action.payload.data)
      return [...state];

    default:
      return state;
  }
};
