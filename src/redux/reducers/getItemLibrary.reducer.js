import * as types from "../../type/typeAction";
const initialState = {
  id: Number,
  playlistsItem: String,
  namePlaylistInput: String,
  avatarPlaylist: String,
};
export const getItemLibraryReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_ITEM_LIBRARY_SUCCESS:
      return action.payload;
    case types.DELETE_LIST_PLAYLIST_SUCCESS:
      return {
        playlistsItem: action.payload,
      };
    default:
      return state;
  }
};
