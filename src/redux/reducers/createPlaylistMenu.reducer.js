import * as types from "../../type/typeAction";
const initialState = [];
export const getPlaylistMenuReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_CREATE_PLAYLIST_SUCCESS: {
      return action.payload.data;
    }

    case types.POST_CREATE_PLAYLIST_SUCCESS: {
      state.push(action.payload);
      return [...state];
    }
    case types.DELETE_LIST_PLAYLIST_SUCCESS: {
      const index = state.findIndex((item) => item.id === action.payload);

      state.splice(index, 1);
      return [...state];
    }
    case types.PUT_PLAYLIST_SUCCESS: {
     
      const index = state.findIndex((item) => item.id === action.payload);
      state.splice(index, 1, action.payload);
      return [...state];
    }
    default:
      return state;
  }
};
