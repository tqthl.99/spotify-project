import * as types from '../../type/typeAction'
const initialStateSearch ={
    categories: {
      
      
    },
  }
export const getCategoriesReducer = (state = initialStateSearch, action) => {
    switch (action.type) {
      case types.GET_CATEGORIES_SUCCESS:
        return {
          ...state,
          categories: action.payload,
        };
      default:
        return state;
    }
  };