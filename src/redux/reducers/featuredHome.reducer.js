import * as types from "../../type/typeAction";
const initialStateHome = {
  featuredPlaylist: [],
};
export const getPlaylistHomeReducer = (state = initialStateHome, action) => {
  switch (action.type) {
    case types.GET_FEATURED_PLAYLIST_SUCCESS:
      return {
        ...state,
        featuredPlaylist: action.payload,
      };

    default:
      return state;
  }
};
