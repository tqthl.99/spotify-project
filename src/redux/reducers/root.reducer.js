import { combineReducers } from "redux";
import { getUserReducer } from "./userHome.reducer";
import {getPlaylistHomeReducer} from './featuredHome.reducer'
import {postPlayMusicHomeReducer} from './playMusicHome.reducer'
import {getCategoriesReducer} from './categoriesSearch.reducer'
import { getSearchListReducer } from "./search.reducer";
import {getPlaylistMenuReducer} from './createPlaylistMenu.reducer'
import {getItemLibraryReducer} from './getItemLibrary.reducer'
import {getTrackPlaylistReducer, postTrackPlaylistReducer} from './getTrackPlaylist.reducer'
export const rootReducer = combineReducers({
  user: getUserReducer,
  featuredPlaylist: getPlaylistHomeReducer,
  songHome: postPlayMusicHomeReducer,
  categories: getCategoriesReducer,
  searchList: getSearchListReducer,
  playlist: getPlaylistMenuReducer,
  playlistLibrary:getItemLibraryReducer,
  trackPlaylist: getTrackPlaylistReducer,
  trackId: postTrackPlaylistReducer,
  
});
