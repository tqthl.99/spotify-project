import * as types from '../../type/typeAction'
const initialStateSearchList ={
    searchList: {
      
      
    },
  }
export const getSearchListReducer = (state = initialStateSearchList, action) => {
    switch (action.type) {
      case types.GET_SEARCH_SUCCESS:
        return {
          ...state,
          searchList: action.payload,
        };
      default:
        return state;
    }
  };