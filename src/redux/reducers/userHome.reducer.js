import * as types from "../../type/typeAction";
const initialState = {
  user: {},
};

export const getUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
      };
    default:
      return state;
      
  }
};
