import * as types from '../../type/typeAction'
  export function deletePlaylistMenuAction(playlist) {
    return {
      type: types.DELETE_LIST_PLAYLIST_SUCCESS,
      payload: playlist,
    };
  }