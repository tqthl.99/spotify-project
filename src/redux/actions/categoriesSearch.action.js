import * as types from "../../type/typeAction";

export const getCategories = () => ({
  type: types.GET_CATEGORIES_REQUEST,
});
export function getCategoriesAction(categories) {
  return {
    type: types.GET_CATEGORIES_SUCCESS,
    payload: categories,
  };
}
