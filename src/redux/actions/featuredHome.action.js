import * as types from "../../type/typeAction";
///
export const getFeaturedPlaylist = () => ({
    type: types.GET_FEATURED_PLAYLIST_REQUEST,
  });
  export function getFeaturedPlaylistAction(featuredPlaylist){
    return {
      type: types.GET_FEATURED_PLAYLIST_SUCCESS,
      payload: featuredPlaylist,
    };
  }
