import * as types from "../../type/typeAction";

export const fetchUser = () => ({ type: types.GET_USER_REQUESTED });
export function getUserAction(user){
  return {
    type: types.GET_USER_SUCCESS,
    payload: user,
  };
}


