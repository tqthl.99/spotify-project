import * as types from "../../type/typeAction";

export const getSearchList = () => ({
    type: types.GET_SEARCH_REQUEST,
  });
  export function getSearchListAction(searchList){
    return {
      type: types.GET_SEARCH_SUCCESS,
      payload: searchList,
    };
  }