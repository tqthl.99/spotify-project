import * as types from "../../type/typeAction";
export const postPlaylist = () => ({
  type: types.POST_CREATE_PLAYLIST_REQUEST,
});
export function postPlaylistAction(playlist) {
  return {
    type: types.POST_CREATE_PLAYLIST_SUCCESS,
    payload: playlist,
  };
}

export const gettPlaylistAction = () => ({
  type: types.GET_CREATE_PLAYLIST_REQUEST,
});
export function getPlaylistMenuAction(playlist) {
  return {
    type: types.GET_CREATE_PLAYLIST_SUCCESS,
    payload: playlist,
  };
}
export function deletePlaylistAction(playlist) {
  return {
    type: types.DELETE_LIST_PLAYLIST_SUCCESS,
    payload: playlist,
  };
}
export function putPlaylistAction(playlist){
  return{
    type:types.PUT_PLAYLIST_SUCCESS,
    payload: playlist,
  }
}
