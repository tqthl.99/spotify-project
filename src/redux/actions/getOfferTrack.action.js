import * as types from "../../type/typeAction";

export const getTrackPlaylistRequest = () => ({
  type: types.GET_OFFER_TRACK_REQUEST,
});
export function getTrackPlaylistAction(track) {
  return {
    type: types.GET_OFFER_TRACK_SUCCESS,
    payload: track,
  };
}

export const postTrackPlaylistRequest = () => ({
  type: types.POST_TRACK_TO_PLAYLIST_REQUEST,
});
export function postTrackPlaylistAction(trackId){
  return {
    type: types.POST_TRACK_TO_PLAYLIST_SUCCESS,
    payload: trackId,
  };
}