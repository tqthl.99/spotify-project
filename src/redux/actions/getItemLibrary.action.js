import * as types from "../../type/typeAction";

export const getPlaylistOfLibraryRequest = () => ({ type: types.GET_ITEM_LIBRARY_REQUEST });
export function getPlaylistOfLibraryAction(playlistLibrary){
  return {
    type: types.GET_ITEM_LIBRARY_SUCCESS,
    payload: playlistLibrary,
  };
}