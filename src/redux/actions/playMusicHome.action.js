import * as types from "../../type/typeAction";


  export function postSongHomeToStoreAction(playSong){
      
    return {
      type: types.POST_PLAY_SONG_HOME_TO_STORE,
      payload: playSong,
    };
  }