import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import InputSearch from "../InputSearch/index";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./style.css";

export default function index() {
  return (
    <div className="header" style={{ backgroundColor: `#000` }}>
      <Container className="header">
        <Switch>
          <Route path="/search">
            <InputSearch />
          </Route>
        </Switch>
      </Container>
    </div>
  );
}
