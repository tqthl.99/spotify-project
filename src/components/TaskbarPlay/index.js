import React, { useRef, useEffect, useState } from "react";
import "./style.css";

import { useSelector } from "react-redux";

const TaskbarPLay = () => {
  // const myFrame = useRef(null);
  const [srcSong, setsrcSong] = useState("");

  const songHomeToStore = useSelector((state) => state.songHome);
  useEffect(() => {
    if (songHomeToStore.includes("https://open.spotify.com/")) {
      let embedSongHomeToStore = songHomeToStore
        .split("https://open.spotify.com/")
        .join()
        .replace(/^,/, "");

      setsrcSong(`https://open.spotify.com/embed/${embedSongHomeToStore}`);
      localStorage.setItem(
        "song",
        `https://open.spotify.com/embed/${embedSongHomeToStore}`
      );
    }
  }, [songHomeToStore]);
  useEffect(() => {
    if (localStorage.getItem("song")) {
      setsrcSong(localStorage.getItem("song"));
    }
  }, []);
  return (
    <div className="taskbar-play">
      {srcSong ? (
        <iframe
          src={srcSong}
          width="100%"
          height="80"
          frameBorder="0"
          allowtransparency="true"
          allow="encrypted-media"
        ></iframe>
      ) : null}
    </div>
  );
};

export default TaskbarPLay;
