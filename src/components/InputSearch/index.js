import React, { useState, useEffect, useRef } from "react";
import { useHistory, useParams } from "react-router-dom";
import "./style.css";

export default function InputForm() {
  const [searchTerm, setSearchTerm] = useState("");
  const history = useHistory();
  const typingTimeOutRef = useRef(null);

  const handleSearchTermChange = (e) => {
    setSearchTerm(e.target.value);

    if (typingTimeOutRef.current) {
      clearTimeout(typingTimeOutRef.current);
    }
    typingTimeOutRef.current = setTimeout(() => {
      history.replace(`/search/${e.target.value}`);
    }, 300);
  };

  const handleClearValueInput = () =>{
    setSearchTerm("")
    history.replace(`/search/`);
  }
  
  return (
    <div>
      <div>
        <div className="input-search-bg">
          <i className="fas fa-search"></i>
          <input
            className="input-search"
            placeholder="Nghệ sĩ, bài hát hoặc podcast"
            value={searchTerm}
            onChange={handleSearchTermChange}
          />
          <div onClick={handleClearValueInput}>
            <i className="fas fa-times"></i>
          </div>
        </div>
      </div>
    </div>
  );
}
