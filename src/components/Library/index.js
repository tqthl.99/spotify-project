import React, { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { getPlaylistOfLibraryRequest } from "../../redux/actions/getItemLibrary.action";
import { Link } from "react-router-dom";

import "./style.css";
import DropdownUser from "../Dropdown/index";
const Library = () => {
  const getPlaylist = useSelector((state) => state.playlist);
  const dispatch = useDispatch();
  const playlistLibrary = useSelector((state) => state.playlistLibrary);
  useEffect(() => {
    dispatch(getPlaylistOfLibraryRequest());
  }, []);

  const { data } = playlistLibrary;

  return (
    <div className="library-component">
      <DropdownUser />

      <div className="title-playlist">
        <h3>Playlists</h3>
      </div>
      <Row className="list-playlist-library">
        {data?.map((item, index) => {
          return (
            <Col xl={{ span: 2 }} key={index}>
              <Link to={`/choose-playlist/${item.id}`}>
                <div className="item-playlist-library">
                  {item.avatarPlaylist === undefined ? (
                    <i className="fas fa-music icon-fa-music"></i>
                  ) : (
                    <img src={item.avatarPlaylist}></img>
                  )}

                  <div className="name-playlist">
                    {item.namePlaylistInput === undefined
                      ? item.playlistsItem + item.id
                      : item.namePlaylistInput}
                  </div>
                  <div className="name-user-playlist">Của Hải Đức</div>
                </div>
              </Link>
            </Col>
          );
        })}
      </Row>
    </div>
  );
};

export default Library;
