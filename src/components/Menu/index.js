import React, { useEffect, useState } from "react";
import "./style.css";
import { Col, Row } from "react-bootstrap";
import { Link, useLocation } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import {
  postPlaylistAction,
  gettPlaylistAction,
} from "../../redux/actions/createPlaylistMenu.action";
import axios from "axios";

const Menu = () => {
  const location = useLocation();
  const playlist = useSelector((state) => state.playlist);
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(gettPlaylistAction());
  }, []);

  const createPlaylist = () => {
    axios
      .post("https://60d0383b7de0b20017107f0a.mockapi.io/playlist", {
        playlistsItem: "playlist của tôi #",
      })
      .then((result) => {
        dispatch(postPlaylistAction(result.data));
      });
  };

  return (
    <div>
      <Row className="menu">
        <div className="menu-fixed">
          <Link to="" >
            <Col className="logo">
              <img src="https://logodownload.org/wp-content/uploads/2016/09/spotify-logo-branca-white.png"></img>
            </Col>
          </Link>
          <Col className={location.pathname == "/" ? "menu-active" : null}>
            <Link to="/" className="d-flex item-menu">
              <i className="fas fa-home"> </i>
              <p>Trang chủ</p>
            </Link>
          </Col>
          <Col className={location.pathname == "/search" ? "menu-active" : null}>
            <Link to="/search" className="d-flex item-menu">
              <i className="fas fa-search"> </i>
              <p>Tìm kiếm</p>
            </Link>
          </Col>
          <Col className={location.pathname == "/library-app" ? "menu-active" : null}>
            <Link
              to="/library-app"
              className="d-flex item-menu library-menu-mobile"
            >
              <i className="fas fa-book"> </i>
              <p>Thư viện</p>
            </Link>
          </Col>
          <Col onClick={createPlaylist} className="create-playlist">
            <Link to="" className="d-flex item-menu">
              <i className="far fa-plus-square"></i>
              <p>Tạo playlist</p>
            </Link>
          </Col>
          <Col className="line-menu"></Col>
          <Col className="list-item-playlist-menu">
            {playlist?.map((item, index) => {
              return (
                <Link to={`/choose-playlist/${item.id}`} key={index}>
                  <div className="child-item-playlist-menu">
                    {item.namePlaylistInput === undefined
                      ? item.playlistsItem + item.id
                      : item.namePlaylistInput}
                  </div>
                </Link>
              );
            })}
          </Col>
          <Col sm={2} className="download-menu">
            <Link to="" className="d-flex item-menu">
              <i className="fab fa-spotify"></i>
              <p>Tải ứng dụng</p>
            </Link>
          </Col>
        </div>
      </Row>
    </div>
  );
};

export default Menu;
