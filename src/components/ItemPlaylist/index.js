import React, { useEffect, useState, useRef } from "react";
import { Formik } from "formik";
import { Modal, Button, Col, Row, Container } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import "./style.css";
import axios from "axios";
import DropdownUser from "../Dropdown/index";
import {
  getTrackPlaylistRequest,
  postTrackPlaylistAction,
} from "../../redux/actions/getOfferTrack.action";
import {
  deletePlaylistAction,
  putPlaylistAction,
} from "../../redux/actions/createPlaylistMenu.action";
import { useSelector, useDispatch } from "react-redux";
import { postSongHomeToStoreAction } from "../../redux/actions/playMusicHome.action";
import { axiosToken } from "../../apis/callApi-test";

const ItemPlaylist = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const track = useSelector((state) => state.trackPlaylist);
  const getTrackAdd = useSelector((state) => state.trackId);
  const getPlaylist = useSelector((state) => state.playlist);
  const [getSongAdd, setgetSongAdd] = useState([]);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [urlAvatar, seturlAvatar] = useState("");
  const [playlist, setplaylist] = useState([]);
  const [editNamePlaylist, seteditNamePlaylist] = useState({});
  const [getTrack, setgetTrack] = useState([]);
  const [checkLike, setcheckLike] = useState(false);
  let { id } = useParams();

  useEffect(() => {
    fetch(`https://60d0383b7de0b20017107f0a.mockapi.io/playlist/${id}`)
      .then((res) => res.json())
      .then((result) => {
        setplaylist(result);
        seturlAvatar(result.avatarPlaylist);
        seteditNamePlaylist(result);

        setgetTrack(result.tracks);
      });

    dispatch(getTrackPlaylistRequest());
  }, [id]);
  const uploadAvatar = (event) => {
    const getAvatar = event.target.files[0];

    const formAvatar = new FormData();
    formAvatar.append("file", getAvatar);
    formAvatar.append("upload_preset", "spotify-app");
    axios
      .post(
        "https://api.cloudinary.com/v1_1/dbxkmzese/image/upload",
        formAvatar
      )
      .then((result) => {
        seturlAvatar(result.data.url);
      });
  };
  const deletePlaylist = async () => {
    try {
      const res = await axios.delete(
        `https://60d0383b7de0b20017107f0a.mockapi.io/playlist/${id}`
      );

      if (res.status == 200) {
        dispatch(deletePlaylistAction(res.data.id));

        history.push("/");
      }
    } catch (err) {
      if (err) {
        // console.log(err)
      }
    }
  };

  return (
    <div className="item-playlist">
      <DropdownUser />
      <div className="header-item-playlist">
        <div className="icon-item-playlist" onClick={handleShow}>
          {urlAvatar === undefined ? (
            <i className="fas fa-music"></i>
          ) : (
            <img src={urlAvatar} className="avatar-Playlist"></img>
          )}
        </div>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Edit details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Formik
              initialValues={{
                namePlaylistInput: `${playlist.playlistsItem}${playlist.id}`,
                descriptionPlaylistInput: "Add an optional description",
              }}
              onSubmit={(values, actions) => {
                setTimeout(() => {
                  // alert(JSON.stringify(values, null, 2));

                  const config = {
                    namePlaylistInput: `${values.namePlaylist}`,
                    descriptionPlaylistInput: `${values.descriptionPlaylist}`,
                    avatarPlaylist: urlAvatar,
                  };

                  axios
                    .put(
                      `https://60d0383b7de0b20017107f0a.mockapi.io/playlist/${id}`,
                      config
                    )
                    .then((res) => {
                      seteditNamePlaylist(res.data);
                      dispatch(putPlaylistAction(res.data));
                    });

                  actions.setSubmitting(false);
                }, 1000);
              }}
            >
              {(props) => (
                <form onSubmit={props.handleSubmit}>
                  <div className="parent-form-modal">
                    <Col>
                      <label
                        htmlFor="file-upload"
                        className="custom-file-upload"
                      >
                        {urlAvatar == undefined ? (
                          <i className="fas fa-music"></i>
                        ) : (
                          <img src={urlAvatar}></img>
                        )}
                      </label>
                      <input
                        id="file-upload"
                        className="input-avatar"
                        type="file"
                        // onChange={props.handleChange}
                        onChange={uploadAvatar}
                        onBlur={props.handleBlur}
                        value={props.values.avatar}
                        name="avatar"
                      />
                    </Col>
                    <Col>
                      <input
                        className="input-name-playlist"
                        type="text"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.namePlaylist}
                        name="namePlaylist"
                      />
                      <input
                        className="input-name-description"
                        type="text"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        // value={props.values.descriptionPlaylist}
                        placeholder={props.values.descriptionPlaylistInput}
                        name="descriptionPlaylist"
                      />
                    </Col>
                  </div>
                  <Modal.Footer>
                    <Button
                      variant="light"
                      type="submit"
                      className="button-submit-modal"
                      onClick={handleClose}
                    >
                      Lưu
                    </Button>
                  </Modal.Footer>
                </form>
              )}
            </Formik>
            {/* <Modal.Footer>
            <Button variant="light" type="submit" onClick={handleClose}>
              Lưu
            </Button>
          </Modal.Footer> */}
          </Modal.Body>
        </Modal>
        <div className="content-header-item-playlist">
          <div className="child-content-header">PLAYLIST</div>
          <div className="child-big-content-header">
            {editNamePlaylist?.namePlaylistInput === undefined
              ? playlist.playlistsItem + playlist.id
              : editNamePlaylist?.namePlaylistInput}
          </div>
          <div className="description-playlist">
            {editNamePlaylist?.descriptionPlaylistInput === undefined
              ? ""
              : editNamePlaylist?.descriptionPlaylistInput}
          </div>
          <div className="child-content-header parent-button-delete">
            <div>Hải Đức</div>
            <div className="button-delete" onClick={deletePlaylist}>
              x
            </div>
          </div>
        </div>
      </div>

      <div className="list-add-track">
        <Col xl={5} className="title-add-track">Tiêu đề</Col>
        <Col xl={5}>Ngày ra mắt</Col>
      </div>

      <div className="offer-item-playlist">
        {getTrack.map((item, index) => {
          return (
            <Row className="item-track" key={index}>
              <Col xl={5} className="parent-name-track">
                <div className="content-img-track">
                  <img src={item?.images[2].url}></img>

                  <i
                    className="fas fa-play-circle"
                    onClick={() => {
                      dispatch(
                        postSongHomeToStoreAction(item.external_urls.spotify)
                      );
                    }}
                  ></i>
                </div>
                <div className="child-name-track">
                  <div className="name-track">{item.name}</div>
                  <div className="parent-name-artist">
                    {item?.artists?.map((content, num) => {
                      return (
                        <div className="child-name-artist" key={num}>
                          {content.name}
                        </div>
                      );
                    })}
                  </div>
                </div>
              </Col>
              <Col xl={5} className="release-date">
                {item?.release_date}
              </Col>
              {/* <Col xl={2} className="like-song">
                {console.log(playlist)}
                <i
                  className={`fas fa-heart ${
                    playlist.statusLike == true && playlist.id == id
                      ? "green-like"
                      : ""
                  }`}
                  onClick={() => {
                    
                    axios
                      .put(
                        `https://60d0383b7de0b20017107f0a.mockapi.io/playlist/${id}`,
                        {
                          album_group: 'double',
                        }
                      )
                      .then((res) => {
                        console.log(res.data.album_group);
                        setplaylist(true);
                      });
                  }}
                ></i>
              </Col> */}
            </Row>
          );
        })}
      </div>

      <div className="offer-item-playlist">
        <div className="title-offer-item-playlist">Đề xuất</div>
      </div>

      <div className="offer-item-playlist">
        {track?.data?.items?.map((item, index) => {
          return (
            <Row className="item-track" key={index}>
              <Col xl={5} className="parent-name-track">
                <div className="content-img-track">
                  <img src={item.images[2].url}></img>
                  <i
                    className="fas fa-play-circle"
                    onClick={() => {
                      dispatch(
                        postSongHomeToStoreAction(item.external_urls.spotify)
                      );
                    }}
                  ></i>
                </div>
                <div className="child-name-track">
                  <div className="name-track">{item.name}</div>
                  <div className="parent-name-artist">
                    {item.artists.map((content, num) => {
                      return (
                        <div className="child-name-artist" key={num}>
                          {content.name}
                        </div>
                      );
                    })}
                  </div>
                </div>
              </Col>

              <Col xl={5} className="release-date">
                {item.release_date}
              </Col>
              <Col
                xl={2}
                className="add-track-playlist"
                onClick={() => {
                  const newArr = [...new Set([...getTrack, item])];
                  axios
                    .put(
                      `https://60d0383b7de0b20017107f0a.mockapi.io/playlist/${id}`,
                      {
                        tracks: newArr,
                      }
                    )
                    .then((res) => {
                      if (res.status == 200) {
                        // console.log(res.data.tracks);
                        // setgetTrack(newArr);
                        setgetTrack(newArr);

                        // setgetTrack([...getTrack]);
                      }

                      dispatch(postTrackPlaylistAction(res));
                    });
                }}
              >
                Thêm
              </Col>
            </Row>
          );
        })}
      </div>
    </div>
  );
};

export default ItemPlaylist;
