import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { fetchUser } from "../../redux/actions/userHome.action";
import { useSelector, useDispatch } from "react-redux";
import { getCategories } from "../../redux/actions/categoriesSearch.action";
import { getSearchList } from "../../redux/actions/search.action";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import DropdownUser from '../Dropdown/index'
import Header from "../Header/index";
import Categories from "./categories/index";
import SearchList from "./SearchList/index";

export default function Search() {
  const [categoriesSearch, setCategoriesSearch] = useState({});

  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.user);
  const categories = useSelector((state) => state.categories.categories);
  const searchList = useSelector((state) => state.searchList.searchList);
  useEffect(() => {
    dispatch(fetchUser());
    dispatch(getCategories());
    dispatch(getSearchList());
  }, []);
  
  return (
    <div>
      <Router>
        <Header />
        <DropdownUser/>
        <div className="home-component">
          <Switch>
            <Route exact path="/search">
              <Categories categories={categories} />
            </Route>
            <Route path="/search/:params">
              <SearchList searchListProp = {searchList}/>
            </Route >
          </Switch>
        </div>
      </Router>
    </div>
  );
}
