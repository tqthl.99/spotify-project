import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { postSongHomeToStoreAction } from "../../../redux/actions/playMusicHome.action";
import "./style.css";

export default function SearchList(props) {
  let { params } = useParams();
  const dispatch = useDispatch();

  
  return (
    <div>
      <section className="search-component">
        <h3 className="message-home-component">
          {props?.searchListProp?.data?.artists?.items[0]?.type}
        </h3>
        <Row className="search-item">
          {props?.searchListProp?.data?.artists?.items
            .filter((item) =>
              item.name.toLowerCase().includes(params.toLowerCase())
            )
            .map((filterArtists, index) => {
              return (
                <Col lg={2} className="search-item-bg" key={index}>
                  <div className="item-img">
                    <img src={filterArtists?.images[1]?.url} />
                  </div>
                  <div>
                    <i
                      className="fas fa-play-circle button-play-home-component"
                      onClick={() => {
                        dispatch(
                          postSongHomeToStoreAction(
                            filterArtists.external_urls.spotify
                          )
                        );
                      }}
                    ></i>
                  </div>
                  <div className="item-context">
                    <div className="item-context-name">
                      {filterArtists.name}
                    </div>
                    <span className="item-contdescription">
                      {filterArtists.type}
                    </span>
                  </div>
                </Col>
              );
            })}
        </Row>
      </section>
      <section>
        <h3 className="message-home-component">
          {props?.searchListProp?.data?.tracks?.items[0]?.type}
        </h3>
        <Row className="search-item">
          {props?.searchListProp?.data?.tracks?.items
            .filter(
              (item) =>
                item.album.name.toLowerCase().includes(params.toLowerCase()) ||
                item.artists[0].name
                  .toLowerCase()
                  .includes(params.toLowerCase()) ||
                item.album.name.toLowerCase().includes(params.toLowerCase())
            )
            .map((filterTrack, index) => {
              return (
                <Col lg={2} className="search-item-bg" key={index}>
                  <div className="item-img">
                    <img src={filterTrack?.album?.images[1]?.url} />
                  </div>
                  <div>
                    <i
                      className="fas fa-play-circle button-play-home-component"
                      onClick={() => {
                        dispatch(
                          postSongHomeToStoreAction(
                            filterTrack.external_urls.spotify
                          )
                        );
                      }}
                    ></i>
                  </div>
                  <div className="item-context">
                    <div className="item-context-name">
                      {filterTrack.album.name}
                    </div>
                    <span className="item-context-description">
                      {filterTrack.album.album_type}
                    </span>
                  </div>
                </Col>
              );
            })}
        </Row>
      </section>
      <section>
        <h3 className="message-home-component">
          {props?.searchListProp?.data?.playlists?.items[0]?.type}
        </h3>
        <Row className="search-item">
          {props?.searchListProp?.data?.playlists?.items
            .filter((item) =>
              item.name.toLowerCase().includes(params.toLowerCase())
            )
            .map((filterPlayList, index) => {
              return (
                <Col lg={2} className="search-item-bg" key={index}>
                  <div className="item-img">
                    <img src={filterPlayList?.images[1]?.url} />
                  </div>
                  <div>
                    <i
                      className="fas fa-play-circle button-play-home-component"
                      onClick={() => {
                        dispatch(
                          postSongHomeToStoreAction(
                            filterPlayList.external_urls.spotify
                          )
                        );
                      }}
                    ></i>
                  </div>
                  <div className="item-context">
                    <div className="item-context-name">
                      {filterPlayList.name}
                    </div>
                    <span className="item-context-description">
                      {filterPlayList.description}
                    </span>
                  </div>
                </Col>
              );
            })}
        </Row>
      </section>
    </div>
  );
}
