import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { randomColor } from "../../../helpers/randomColor";
import { Link } from "react-router-dom";
import "./style.css";

export default function Categories(props) {
  return (
    <div className="search-categories">
      <Row>
        <h3 className="message-home-component">Duyệt tìm tất cả</h3>
      </Row>
      <Row>
        {props?.categories?.data?.categories?.items.map((item, index) => {
          return (
            <Col key={index} lg={2}>
              <Link to="" style={{ textDecoration: "none" }}>
                <div
                  className="search-categories-bg"
                  style={{
                    backgroundColor: `${randomColor()}`,
                  }}
                >
                  <div>
                    <h4>{item.name}</h4>
                    <div>
                      {item.icons.map((icon) => {
                        return (
                          <img className="categories-icon" src={icon.url} />
                        );
                      })}
                    </div>
                  </div>
                </div>
              </Link>
            </Col>
          );
        })}
      </Row>
    </div>
  );
}
