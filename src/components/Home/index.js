import React, { useEffect, useState } from "react";
import { Row, Col, Dropdown } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { fetchUser } from "../../redux/actions/userHome.action";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { getFeaturedPlaylist } from "../../redux/actions/featuredHome.action";
import { postSongHomeToStoreAction } from "../../redux/actions/playMusicHome.action";
import DropdownUser from "../Dropdown/index";
import "./style.css";

const Home = () => {
  
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.user);
  const featuredPlaylist = useSelector(
    (state) => state.featuredPlaylist.featuredPlaylist
  );

  useEffect(() => {
    dispatch(fetchUser());
    dispatch(getFeaturedPlaylist());
  }, []);
  
  return (
    <div className="home-component">
      <DropdownUser />

      {featuredPlaylist?.map((item, index) => {
        const { message, playlists } = item.data;
        return (
          <div key={index}>
            <h3 className="message-home-component">{message}</h3>
            <div className="home-featured-parent">
              {playlists.items.map((content, i) => {
                return (
                  <div key={i}>
                    <Link to="" className=" link-home-component">
                      <div className="featured-playlist-item-home">
                        <div>
                          <img
                            className="img-featured-playlist-home"
                            src={content.images[0].url}
                          ></img>
                          <div>
                            <i
                              className="fas fa-play-circle button-play-home-component"
                              onClick={() => {
                                dispatch(
                                  postSongHomeToStoreAction(
                                    content.external_urls.spotify
                                  )
                                );
                              }}
                            ></i>
                          </div>
                        </div>
                        <div className="featured-playlist-home-name">
                          {content.name}
                        </div>
                        <div className="featured-playlist-home-description">
                          {content.description}
                        </div>
                      </div>
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Home;
