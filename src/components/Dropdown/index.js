import React from "react";
import { Row, Col, Dropdown } from "react-bootstrap";
import "./style.css";
const DropdownUser = () => {
  return (
    <div className='container-user'>
      <div className="dropdown-user">
        <Dropdown className="dropdown-user-item">
          <Dropdown.Toggle variant="success" id="dropdown-basic">
            <img src="https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=1670693576350832&height=300&width=300&ext=1627097436&hash=AeSNjjQToHrBecet-LE"></img>
            <p>Trần Toàn</p>

            
            <i className="fas fa-chevron-down"></i>
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item href="">Tài khoản</Dropdown.Item>
            <Dropdown.Item href="">Hồ sơ</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </div>
  );
};

export default DropdownUser;
