import logo from "./logo.svg";
import "./App.css";
import { Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import Home from "./components/Home/index";
import Menu from "./components/Menu/index";
import TaskbarPlay from "./components/TaskbarPlay/index";
import Search from "./components/Search/index";

import Library from "./components/Library/index";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ItemPlaylist from "./components/ItemPlaylist/index";

function App() {
  return (
    <Provider store={store}>
      <Container fluid className="app">
        <Router>
          <div className="menu-home-search-library">
            <Menu className="menu-app" />
            <div className="page-menu-home-search-library">
              <Switch>
                <Route path="/search">
                  <Search className="container-search" />
                </Route>
                <Route path="/library-app">
                  <Library className="" />
                </Route>
                <Route path="/choose-playlist/:id">
                  <ItemPlaylist className="item-playlist" />
                </Route>
                <Route path="/">
                  <Home className="home-app" />
                </Route>
              </Switch>
            </div>
          </div>
        </Router>
      </Container>
      <TaskbarPlay className="taskbar-play-app" />
    </Provider>
  );
}

export default App;
