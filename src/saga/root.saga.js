import { fork, all } from "redux-saga/effects";
import * as homeSaga from "./home.saga";
import * as createPlaylist from "./createPlaylist.saga";
import * as librarySaga from "./library.saga";
import * as searchSaga from "./search.saga";

import * as trackPlaylistSaga from './trackPlaylist.saga'
const rootSaga = function* () {
  yield all([
    homeSaga.watcherGetUser(),
    homeSaga.watcherGetFeaturedPlaylist(),
    createPlaylist.watcherPostPlaylist(),
    //    createPlaylist.watcherGetPlaylist(),
    searchSaga.watcherGetCategories(),
    searchSaga.watcherGetSearchList(),
       createPlaylist.watcherGetPlaylist(),
    librarySaga.watcherGetPlaylist(),
    trackPlaylistSaga.watcherGetTrack(),
  ]);
};
export default rootSaga;
