import * as types from "../type/typeAction";
import { call, put, takeLatest } from "redux-saga/effects";

import axios from "axios";
import { getPlaylistOfLibraryAction } from "../redux/actions/getItemLibrary.action";

function* workerGetPlaylist() {
  try {
    const playlist = yield call(() =>
      axios.get("https://60d0383b7de0b20017107f0a.mockapi.io/playlist")
    );

    yield put(getPlaylistOfLibraryAction(playlist));
  } catch {}
}
export function* watcherGetPlaylist() {
  yield takeLatest(types.GET_ITEM_LIBRARY_REQUEST, workerGetPlaylist);
}
