import { call, put, takeLatest } from "redux-saga/effects";

import * as types from "../type/typeAction";
import qs from "querystring";
import axios from "axios";
import { axiosToken } from "../apis/callApi-test";
import { getUserAction } from "../redux/actions/userHome.action";
import { getFeaturedPlaylistAction } from "../redux/actions/featuredHome.action";
const URL_ME = "https://api.spotify.com/v1/me";
const URL_HOME =
  "https://api.spotify.com/v1/browse/featured-playlists?country=VN&locale=sv_VN&timestamp=2021-5-23T09%3A00%3A00.000Z&limit=6&offset=5";
const URL_HOME1 =
  "https://api.spotify.com/v1/browse/featured-playlists?country=VN&locale=vi_VN&timestamp=2021-6-23T09%3A00%3A00.000Z&limit=6&offset=5";
const URL_HOME2 =
  "https://api.spotify.com/v1/browse/featured-playlists?country=VN&locale=vi_VN&timestamp=2021-7-23T09%3A00%3A00.000Z&limit=6&offset=5";
const URL_HOME3 =
  "https://api.spotify.com/v1/browse/featured-playlists?country=VN&locale=vi_VN&timestamp=2021-8-23T09%3A00%3A00.000Z&limit=6&offset=5";
const URL_HOME4 =
  "https://api.spotify.com/v1/browse/featured-playlists?country=VN&locale=vi_VN&timestamp=2021-9-23T09%3A00%3A00.000Z&limit=6&offset=5";
const URL_HOME5 =
  "https://api.spotify.com/v1/browse/featured-playlists?country=VN&locale=vi_VN&timestamp=2021-10-23T09%3A00%3A00.000Z&limit=6&offset=5";

// fetch user

function* workerGetUser() {
  try {
    const user = yield call(() => axiosToken.get(URL_ME));
    // const playlist = { title: 'React POST Request Example' };
    // axios.post('https://60d0383b7de0b20017107f0a.mockapi.io/sportify', playlist)

    yield put(getUserAction(user));
  } catch (error) {
    yield put({ type: types.GET_USER_FAILED, message: error.message });
  }
}
export function* watcherGetUser() {
  yield takeLatest(types.GET_USER_REQUESTED, workerGetUser);
}

//  playlist
function getUrlHome() {
  return axiosToken.get(URL_HOME);
}
function getUrlHome1() {
  return axiosToken.get(URL_HOME1);
}
function getUrlHome2() {
  return axiosToken.get(URL_HOME2);
}
function getUrlHome3() {
  return axiosToken.get(URL_HOME3);
}
function getUrlHome4() {
  return axiosToken.get(URL_HOME4);
}
function getUrlHome5() {
  return axiosToken.get(URL_HOME5);
}
function* workerGetFeaturedPlaylist() {
  try {
    const featuredPlaylist = yield call(() =>
      Promise.all([
        getUrlHome(),
        getUrlHome1(),
        getUrlHome2(),
        getUrlHome3(),
        getUrlHome4(),
        getUrlHome5(),
      ])
    );

    yield put(getFeaturedPlaylistAction(featuredPlaylist));
  } catch (error) {
    yield put({
      type: types.GET_FEATURED_PLAYLIST_FAILED,
      message: error.message,
    });
  }
}
export function* watcherGetFeaturedPlaylist() {
  yield takeLatest(
    types.GET_FEATURED_PLAYLIST_REQUEST,
    workerGetFeaturedPlaylist
  );
}
