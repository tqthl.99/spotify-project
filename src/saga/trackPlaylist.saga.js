import { call, put, takeLatest } from "redux-saga/effects";

import * as types from "../type/typeAction";
import { axiosToken } from "../apis/callApi-test";
import { getTrackPlaylistAction } from "../redux/actions/getOfferTrack.action";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { BrowserRouter as Router, useParams } from "react-router-dom";

// const getTrackStore = useSelector((state) => state.trackId);
const URL_TRACK =
  "https://api.spotify.com/v1/artists/0TnOYISbd1XYRBk9myaseg/albums?include_groups=single%2Cappears_on&market=ES&limit=10&offset=5";


function* workerGetTrack() {
  try {
    const track = yield call(() => axiosToken.get(URL_TRACK));

    yield put(getTrackPlaylistAction(track));
  } catch {}
}
export function* watcherGetTrack() {
  yield takeLatest(types.GET_OFFER_TRACK_REQUEST, workerGetTrack);
}
