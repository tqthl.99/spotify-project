import { call, put, takeLatest } from "redux-saga/effects";

import * as types from "../type/typeAction";
import qs from "querystring";
import axios from "axios";
import { axiosToken } from "../apis/callApi-test";
import { getCategoriesAction } from "../redux/actions/categoriesSearch.action";
import { getSearchListAction } from "../redux/actions/search.action";
const URL_CATEGORIES =
  "https://api.spotify.com/v1/browse/categories?country=VN&locale=vi_VN&limit=15&offset=5";

const URL_SEARCH =
  "https://api.spotify.com/v1/search?q=Muse&type=track%2Cartist%2Cplaylist&market=US&limit=50&offset=5";

function* workerGetCategories() {
  try {
    const categories = yield call(() => axiosToken.get(URL_CATEGORIES));

    yield put(getCategoriesAction(categories));
  } catch (error) {
    yield put({
      type: types.GET_CATEGORIES_FAILED,
      message: error.message,
    });
  }
}

function* workerGetSearchList() {
  try {
    const searchList = yield call(() => axiosToken.get(URL_SEARCH));

    yield put(getSearchListAction(searchList));
  } catch (error) {
    yield put({
      type: types.GET_SEARCH_FAILED,
      message: error.message,
    });
  }
}

export function* watcherGetCategories() {
  yield takeLatest(types.GET_CATEGORIES_REQUEST, workerGetCategories);
}

export function* watcherGetSearchList() {
  yield takeLatest(types.GET_SEARCH_REQUEST, workerGetSearchList);
}
