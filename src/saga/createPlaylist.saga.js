import axios from "axios";
import * as types from "../type/typeAction";
import { call, put, takeLatest } from "redux-saga/effects";
import { postPlaylistAction, getPlaylistMenuAction } from "../redux/actions/createPlaylistMenu.action";
import React, { useEffect, useState } from "react";
const URL_PLAYLIST = "https://60d0383b7de0b20017107f0a.mockapi.io/playlist";

function* workerPostPlaylist() {
  try {
    
  //  const postPlaylist = yield call(() =>
  //     axios.post("https://60d0383b7de0b20017107f0a.mockapi.io/playlist", {
  //       playlistsItem: "playlist của tôi #",
  //     })
  //   );
  
  } catch {}
}
export function* watcherPostPlaylist() {
  // yield takeLatest(types.POST_CREATE_PLAYLIST_REQUEST, workerPostPlaylist);
}
///////

function* workerGetPlaylist() {
  try {
    const playlist = yield call(() => axios.get(URL_PLAYLIST));
    // const playlist = { title: 'React POST Request Example' };
    // axios.post('https://60d0383b7de0b20017107f0a.mockapi.io/sportify', playlist)

    yield put(getPlaylistMenuAction(playlist));
  } catch{
  }
}
export function* watcherGetPlaylist() {
  yield takeLatest(types.GET_CREATE_PLAYLIST_REQUEST, workerGetPlaylist);
}
