import axios from "axios";
import { access } from "fs";
import qs from "querystring";
import moment from "moment";
const url = "https://accounts.spotify.com/api/token";
const body = {
  grant_type: `${process.env.REACT_APP_GRAND_TYPE_REFRESH_TOKEN}`,
  refresh_token: `${process.env.REACT_APP_REFRESH_TOKEN}`,
  client_id: `${process.env.REACT_APP_CLIENT_ID}`,
  client_secret: `${process.env.REACT_APP_CLIENT_SECRET}`,
};

export const axiosRefreshToken = axios.create({});
export const axiosToken = axios.create({});

axiosToken.interceptors.request.use(
  async (config) => {
    if (localStorage.getItem("token")) {
      const tokenLocal = JSON.parse(localStorage.getItem("token"));
      const { access_token, expires_in } = tokenLocal;

      const expires_inFormat = moment(expires_in)._d;
      if (expires_inFormat < new Date()) {
        const response = await axiosRefreshToken.post(url, qs.stringify(body), {
          headers: {
            "content-type": "application/x-www-form-urlencoded;charset=utf-8",
          },
        })
        if(response.status === 200){
          config.headers.Authorization = `Bearer ${response.data.access_token}`
          return config
        }
      }
      config.headers.Authorization = `Bearer ${access_token}`;
      return config;
    }

    try {
      const response = await axiosRefreshToken.post(url, qs.stringify(body), {
        headers: {
          "content-type": "application/x-www-form-urlencoded;charset=utf-8",
        },
      });

      if (response.status === 200) {
        config.headers.Authorization = `Bearer ${response.data.access_token}`;
      }
    } catch (error) {
      if (error) {
        // console.log(error);
      }
    }

    return config;
  },
  (error) => {
    if (error) return Promise.reject(error);
  }
);

axiosRefreshToken.interceptors.response.use(
  (response) => {
    if (
      response.status === 200 &&
      response.data &&
      response.data.access_token
    ) {
      const { access_token, expires_in } = response.data;
      const afterDate = new Date();
      afterDate.setSeconds(afterDate.getSeconds() + 3600);
      
      const saveLocal = {
        access_token: access_token,
        expires_in: afterDate,
      };
      localStorage.setItem("token", JSON.stringify(saveLocal));
    }
    return response;
  },
  (error) => {
    if (error) {
      // console.log(error);
    }
  }
);
