import axios from "axios";
import qs from "querystring";

export const BASIC =
  "MDEwYjZiMTdkZTczNDllMTkzOTBmNTM2NzJmMTg4Njc6NzM5MWVlM2NlMDJjNDVhY2I5M2E2YTdiM2UxYzk3Nzk=";
export const REFRESH_TOKEN =
  "AQCyVUmH0ROpLlLArTYNhyBnw6xLBZq8YLbnws091ayvfbol1iq9DkqJYztZNM3CmS6IMWSFlN3pqZ3DCSvkp1rM3E7MgYeNfT1coAcrRflhKZfhyKB2mXOjYrWnQ6BPiU0";

export function generateToken() {
  const url = "https://accounts.spotify.com/api/token";

  const body = {
    grant_type: `${process.env.REACT_APP_GRAND_TYPE_REFRESH_TOKEN}`,
    refresh_token: `${process.env.REACT_APP_REFRESH_TOKEN}`,
    client_id: `${process.env.REACT_APP_CLIENT_ID}`,
    client_secret: `${process.env.REACT_APP_CLIENT_SECRET}`,
  };
  const config = {
    headers: {
      "content-type": "application/x-www-form-urlencoded;charset=utf-8",
    },
  };
  return axios.post(url, qs.stringify(body), config);
}

export const getFeaturedPlaylist = (token) => {
  const url =
    "https://api.spotify.com/v1/browse/featured-playlists?country=VN&locale=sv_VN&timestamp=2014-10-23T09%3A00%3A00.000Z&limit=6&offset=5";
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: `Bearer ${localStorage.getItem("access_token")}`,
  };
  return axios.get(url, { headers: headers }).then((response) => {
    return response.data;
  });
};





